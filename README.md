# Overview

Penny farthing is a solitaire card game engine using a web canvas and written in vanilla JavaScript.

The game is implemented in such a way as to make adding new games easy, see [doc/rules](doc/rules.md) on how to do this. You can also view the source for the existing games in the [rules](rules) directory. A high-level overview of the engine implementation is described in the [design overview document](doc/design.md).

# References

* [Vector Playing Cards, by Chris](https://sourceforge.net/projects/vector-cards/)
* [Random number generator source, by Protonk](https://gist.github.com/Protonk/5367430)
* [Riders Image, By L. Prang & Co., touched up by user:Churchh, Public domain, via Wikimedia Commons](https://commons.wikimedia.org/wiki/File%3ABicycling-ca1887-bigwheelers.jpg)

# License

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
