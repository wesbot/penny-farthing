The program is modelled towards a model-view-controller pattern.

* The Model stores all the card data.
* The View manages drawing to the canvas.
* The Controller handles user input and translates it to the methods that changes the model. It also provides methods to manipulate the model and view.

Additionally there are helper objects that make building card games easier:

* The Deck is used to handle a deck of cards.
* The Rules is where the game logic lives. It defines the layout of the play field, and is responsible for dealing the cards, how cards are moved, flipped or taken, and determining the win condition.

Each card game is implemented as a new set of rules. To ensure decent isolation, each rule file is *only allowed* to modify the game through the **controller** object.

# Program Flow

```
+---------+
| DECK.JS |
+-^-------+
  |            +----------------+
  |        +--->  GAME RULES.JS <--+
  |        |   +----------------+  |
  |        |                       |
+-+--------+-+                   +-+-------+
|  MODEL.JS  <-------------------+ VIEW.JS |
+----------+-+                   +-+----+--+
           |                       |    ^
           |   +---------------+   |    |
           +---> CONTROLLER.JS <---+    |
               +-------+-------+        |
                       |                |
                  EVENT|DRIVEN          |
                       |                |
               +-------v-------+        |
               |               +--------+
               |   GAME LOOP   |
               |               |
               +---------------+
```
